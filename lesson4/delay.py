from time import sleep
from functools import wraps

def delay(func):
    @wraps(func)
    def wrapper():
        sleep(6)
        return func
    return wrapper
