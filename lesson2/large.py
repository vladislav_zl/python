#!/usr/bin/env python3
from sys import argv
import math

if len(argv) != 4:
  print("Error")
  exit()

a = int(argv[1])
b = int(argv[2])
c = int(argv[3])

if a + b <= c:
  exit()
elif b + c <= a:
  exit()
elif a + c <= b:
  exit()
else:
  p = (a+b+c) / 2
  S = p * (p-a) *(p-b) *(p-c)
  print(math.sqrt(S))
