import requests
from bs4 import BeautifulSoup


class WikiParser:
    def __init__(self, page):
        self.page = requests.get(page)
        self.soup = BeautifulSoup(self.page.text, 'lxml')
        self.content = self.soup.body.find(id='mw-content-text')

    def lang_list(self):
        list_of_langs = []
        for body_line in self.soup.body.find_all('li'):
            if body_line.has_attr('class') and body_line.a.has_attr('class'):
                list_of_langs.append([body_line.a.string, body_line.a.get('href')])
        return list_of_langs

    def number_of_tables(self):
        counts = 0
        for _ in self.content.find_all('table'):
            counts += 1
        return counts

    def data_from_tables(self):
        table_list = []
        for table in self.content.find_all('table'):
            for table_string in table.tbody.find_all('tr'):
                list_of_string = []
                for data in table_string.find_all("td"):
                    if data.string is not None:
                        list_of_string.append(data.string)
                if list_of_string:
                    table_list.append(list_of_string)
        return table_list

