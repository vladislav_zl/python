import requests
import json
from bs4 import BeautifulSoup


def parse_top_250(file_name):
    get_site = requests.get("https://imdb.com/chart/top", headers={'Accept-Language': 'En-us'})
    soup = BeautifulSoup(get_site.text, 'lxml')
    data = []
    for body_line in soup.tbody.find_all('tr'):
        for line in body_line.find_all('td'):
            if "posterColumn" in line['class']:
                position = line.span.get('data-value')
            if "titleColumn" in line['class']:
                film_name = line.a.string
                year = line.span.string.strip("()")
                director = line.a.get('title').split(', ')[0].split(" (dir.)")[0]
                crew = ', '.join(line.a.get('title').split(', ')[1:])
            if "ratingColumn" in line['class']:
                if line.strong:
                    rating = line.strong.string
        data.append({film_name: {"Position": position,"Year": year,"Director": director,"Crew": crew,"Rating": rating}})
    with open(file_name, 'w') as file_write:
        json.dump(data, file_write, ensure_ascii=False)
