def multiple_in_range(one, two):
    return [i for i in range(one, two + 1) if i % 7 == 0 and i % 5 != 0]
    # my try
    # try:
    #     hop = two + 1
    #     list = []
    #     for i in range(one, hop):
    #         if i % 7 == 0 and i % 5 !=0:
    #             list.append(i)
    #     return(list)
    # except TypeError as e:
    #     raise TypeError