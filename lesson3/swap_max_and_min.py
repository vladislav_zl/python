def swap_max_and_min(mylist):
    for item in mylist:
        if isinstance(item, int):
            continue
        else:
            raise TypeError
    if len(set(mylist)) != len(mylist):
        raise ValueError
    else:
        minimal = min(mylist)
        maximum = max(mylist)
        index_min = mylist.index(minimal)
        index_max = mylist.index(maximum)
        mylist[index_min] =  maximum
        mylist[index_max] = minimal
        return mylist