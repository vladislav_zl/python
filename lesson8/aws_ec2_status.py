import boto3


def convert_status(region, from_state, to_state):
    ec2 = boto3.resource('ec2', region_name=region)
    filters = [{
            'Name': 'instance-state-name',
            'Values': [from_state]
            }]
    eval("ec2.instances.filter(Filters=filters).{}()".format(to_state))
