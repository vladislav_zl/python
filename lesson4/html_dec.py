def italic(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<i>" + str(msg_2) + "</i>"
        return msg
    return wrapper

def bold(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<b>" + str(msg_2) + "</b>"
        return msg
    return wrapper

def underline(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<u>" + str(msg_2) + "</u>"
        return msg
    return wrapper