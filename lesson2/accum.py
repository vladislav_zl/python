#!/usr/bin/env python3
def accum(list):
    print( "-".join([y.upper() + y * x for x, y in enumerate(list) ]))
