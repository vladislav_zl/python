def dict_swap(dictionary):
    return {v: k for k, v in dictionary.items()}

    # new_dict = {}
    # for key, var in dictionary.items():
    #     new_dict[var] = key
    # return new_dict
