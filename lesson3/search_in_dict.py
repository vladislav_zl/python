def search_in_dict(dict1,set1):
    found_items = set()
    for k in dict1:
        if k in set1:
            found_items.add(k)
    return found_items