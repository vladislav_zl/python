#!/usr/bin/env python3

import os
import logging
import requests
import argparse
import boto3
from botocore.exceptions import ClientError
from bs4 import BeautifulSoup
from collections import Counter

DEFAULT_LOG = os.path.splitext(os.path.basename(__file__))[0] + ".log"
DEFAULT_S3_CONF = {'LocationConstraint': 'eu-central-1'}


def copy_to_bucket(to_bucket):
    """Sending logfile to s3 bucket. Take credentials from standard file for system.
    Create bucket if it don't exist."""
    s3 = boto3.client('s3')
    response = s3.list_buckets()
    buckets = [bucket['Name'] for bucket in response['Buckets']]
    if to_bucket not in buckets:
        try:
            s3.create_bucket(Bucket=to_bucket, CreateBucketConfiguration=DEFAULT_S3_CONF)
        except ClientError as e:
            logger.error(e)
    s3.upload_file(logger.name, to_bucket, logger.name)


def find_tags(link) -> tuple:
    """Parser for web-page, find tags, return tuple which contains:
    the total number of tags and the number of each tag"""
    parse_link = requests.get(link).text
    soup = BeautifulSoup(parse_link, "lxml")
    list_of_tags = [tag.name for tag in soup.find_all()]
    tag_dict = Counter()
    for tag in list_of_tags:
        tag_dict[tag] += 1
    summary = sum(tag_dict.values())
    return_dict = dict(tag_dict.most_common())
    logger.info("{} {} {}".format(link, summary, return_dict))
    return summary, return_dict


def parse_args():
    """Parser of arguments."""
    parser = argparse.ArgumentParser(description='Html tags finder.')
    parser.add_argument('link', type=str,
                        help="The link with html tags.")
    parser.add_argument('-l', "--log", type=str, nargs='?', const=DEFAULT_LOG,
                        help="Logging into a local file. Default - ./{}.".format(DEFAULT_LOG))
    parser.add_argument('-b', "--bucket", type=str,
                        help="Sending log to s3 bucket. "
                             "Take credentials from shared file (linux default - ~/.aws/credentials). "
                             "The bucket will be created if it don't exist (default zone '{}'). "
                             "The key '-l'/'--log' is required.".format(DEFAULT_S3_CONF['LocationConstraint']))
    args_pars = parser.parse_args()
    if args_pars.log is None and args_pars.bucket:
        parser.error("The key '-b'/'--bucket' requires key '-l'/'--log'.")
    return args_pars


def get_logger(filename):
    """Initialize the logger. We add logging to the file, if the key '-l'/'--log' was specified.
    The name of logger is taken from the log file name. It used in copy_to_bucket()"""
    logging.basicConfig(level=logging.INFO,
                        format="%(message)s",
                        datefmt='%Y/%M/%m/%H/%d %H:%M',
                        )
    basic_logger = logging.getLogger(filename)
    if filename:
        file_handler = logging.FileHandler("./{}".format(filename))
        log_formatter = logging.Formatter("%(asctime)s %(message)s")
        file_handler.setFormatter(log_formatter)
        basic_logger.addHandler(file_handler)
    return basic_logger


def main(link_name, logfile=None, bucket_name=None):
    """Function to start the program. Passing arguments is required to run."""
    global logger
    logger = get_logger(logfile)
    find_tags(link_name)
    if bucket_name:
        try:
            copy_to_bucket(bucket_name)
        except Exception as e:
            logger.error(e)


if __name__ == '__main__':
    """Function to run the program from the console"""
    args = parse_args()
    main(args.link, args.log, args.bucket)
