import boto3
from sys import argv
from botocore.exceptions import ClientError


def copy_to_bucket(to_bucket, from_buckets):
    s3 = boto3.client('s3')
    response = s3.list_buckets()
    buckets = [bucket['Name'] for bucket in response['Buckets']]
    if to_bucket not in buckets:
        try:
            config = {'LocationConstraint': 'eu-central-1'}
            s3.create_bucket(Bucket=to_bucket, CreateBucketConfiguration=config)
        except ClientError as e:
            print("Exit. {}".format(e))
            exit(1)
    if isinstance(from_buckets, list):
        for bucket in buckets:
            if bucket != to_bucket:
                list_of_files = s3.list_objects_v2(Bucket=bucket)
                if 'Contents' in list_of_files:
                    for file in list_of_files['Contents']:
                        copy_source = {
                            'Bucket': bucket,
                            'Key': file['Key']
                        }
                        s3.copy(copy_source, to_bucket, file['Key'])
    else:
        print("{} should be a list".format(from_buckets))
        exit(1)


if __name__ == "__main__":
    try:
        copy_to_bucket(argv[1], argv[2])
    except IndexError:
        print("We don't have enough keys.")
