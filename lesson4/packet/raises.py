def raises(error):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                x = func(*args, **kwargs)
                return x
            except:
                raise error
        return wrapper
    return decorator