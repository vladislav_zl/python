class Employee:

    def __init__(self, name, surname, salary):
        self.first_name = name.capitalize()
        self.last_name = surname.capitalize()
        self.salary = int(salary)
        self.email = self.first_name.lower() + "_" \
                     + self.last_name.lower() \
                     + "@example.com"

    @property
    def full_name(self):
        return "{}, {}".format(self.first_name, self.last_name)

    @full_name.setter
    def full_name(self, full_name):
        name, surname = full_name.split(", ")
        self.first_name = name.capitalize()
        self.last_name = surname.capitalize()

    @classmethod
    def from_str(cls, employee_str):
        return cls(*employee_str.split(","))

class DevOps(Employee):
    def __init__(self,name, surname, salary, skills=[]):
        super().__init__(name, surname, salary)
        self.skills = [x.lower().capitalize() for x in skills]

    def add_skill(self, skill):
        self.skill = skill.lower().capitalize()
        if self.skill not in self.skills:
            return self.skills.append(self.skill)

    def remove_skill(self, skill):
        self.skill = skill.lower().capitalize()
        if self.skill in self.skills:
            return self.skills.remove(self.skill)


class Manager(Employee):
    def __init__(self, name, surname, salary, subordinates=[]):
        super().__init__(name, surname, salary)
        self.subordinates = subordinates

    def add_subordinate(self, subordinate):
        return self.subordinates.append(subordinate)

    def remove_subordinate(self, for_delete):
        if for_delete in self.subordinates:
            return self.subordinates.remove(for_delete)
        else:
            try:
                for subordinate in self.subordinates:
                    if str(for_delete) in subordinate.__dict__.values():
                         return self.subordinates.remove(subordinate)
            except KeyboardInterrupt as e:
                pass
