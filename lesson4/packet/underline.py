def underline(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<u>" + str(msg_2) + "</u>"
        return msg
    return wrapper