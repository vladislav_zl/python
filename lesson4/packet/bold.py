def bold(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<b>" + str(msg_2) + "</b>"
        return msg
    return wrapper