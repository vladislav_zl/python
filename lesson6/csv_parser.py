import csv
from collections import Counter

class CsvParser:
    def __init__(self, file_name):
        self.file_name = file_name

    def save_as(self, new_file, separator):
        self.new_file = new_file
        self.separator = separator
        with open(self.file_name, 'r') as r_csv:
            csv_reader = csv.reader(r_csv)
            with open(self.new_file, 'w') as w_csv:
                csv_writer = csv.writer(w_csv, delimiter=self.separator)
                csv_writer.writerow(csv_reader)

    def get_country_profit(self, country):
        self.country = country
        count = 0.
        with open(self.file_name, "r") as r_csv:
            csv_reader = csv.DictReader(r_csv)
            for line in csv_reader:
                if line["Country"] == self.country:
                  count = count + float(line["Total Profit"])
        return count

    def sell_over(self, type_product, sell):
        self.type_product = type_product
        self.sell = sell
        list_of_counties = []
        with open(self.file_name, "r") as r_csv:
            csv_reader = csv.DictReader(r_csv)
            for line in csv_reader:
                if line["Item Type"] == self.type_product and int(line["Units Sold"]) > self.sell:
                    list_of_counties.append(line["Country"])
        return sorted(set(list_of_counties))




