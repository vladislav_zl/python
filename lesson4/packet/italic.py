def italic(func):
    def wrapper(*args, **kwargs):
        msg_2 = func(*args, **kwargs)
        msg = "<i>" + str(msg_2) + "</i>"
        return msg
    return wrapper