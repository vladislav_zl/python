from collections import Counter


class LogParser:

    def __init__(self, file_name):
        self.file_name = file_name

    def get_most_common(self, top):
        # my try
        # with open(self.file_name, "r") as log_file:
        #     counts = {}
        #     for line in log_file:
        #         ip = line.split(' ', 1)[0]
        #         if ip not in counts:
        #             counts[ip] = 1
        #         else:
        #             counts[ip] += 1
        # sorted_counts = [(k, counts[k]) for k in sorted(counts, key=counts.get, reverse=True)]
        # return sorted_counts[:top]

        # how it could be
        with open(self.file_name) as log:
            ip_dict = Counter()
            for line in log:
                ip_dict[(line[:line.find(" ")])] += 1
            return ip_dict.most_common(top)

    def log_by_http_code(self, file_to, code):
        with open(self.file_name, "r") as log_file:
            with open(file_to, "w") as output_file:
                for line in log_file:
                    if str(code) in line:
                        output_file.write(line)
