#!/usr/bin/env python3

from sys import argv

try:
    if int(argv[1]) > -15 and int(argv[1]) <= 12:
        print(True)
    elif int(argv[1]) > 14 and int(argv[1]) < 17:
        print(True)
    elif int(argv[1]) >= 19:
        print(True)
    else:
        print(False)
except Exception as no:
    print(no)
